"""
Reference: https://spacy.io/usage/rule-based-matching#_title
"""

time_patterns = [
    [
        {"LEMMA": {"IN": ["in", "on", "at", "before", "after", "since"]}},
        {"POS": "DET", "OP": "?"},
        {"ENT_TYPE": "DATE", "OP": "?"},
        {"ENT_TYPE": "TIME", "OP": "+"}
    ],
    [
        {"LEMMA": {"IN": ["in", "on", "at", "before", "after", "since"]}},
        {"POS": "DET", "OP": "?"},
        {"ENT_TYPE": "TIME", "OP": "+"},
        {"ENT_TYPE": "DATE", "OP": "?"}
    ],
    [
        {"LEMMA": {"IN": ["in", "on", "at", "before", "after", "since"]}},
        {"POS": "DET", "OP": "?"},
        {"ENT_TYPE": "DATE", "OP": "+"},
        {"ENT_TYPE": "TIME", "OP": "?"}
    ],
    [
        {"LEMMA": {"IN": ["in", "on", "at", "before", "after", "since"]}},
        {"POS": "DET", "OP": "?"},
        {"ENT_TYPE": "TIME", "OP": "?"},
        {"ENT_TYPE": "DATE", "OP": "+"}
    ]
]

type_pattern = [
    {
        "RIGHT_ID": "anchor_ticket",
        "RIGHT_ATTRS": {"LEMMA": "ticket"}
    },
    {
        "LEFT_ID": "anchor_ticket",
        "REL_OP": ">",
        "RIGHT_ID": "type_subject",
        "RIGHT_ATTRS": {"LEMMA": {"IN": ["change", "create", "audit", "comment"]}, "DEP": {"IN": ["acl", "relcl"]}}
    }
]


def parse_time_matches(matches, doc):
    start_min = float('inf')
    end_max = - float('inf')
    for _, start, end in matches:
        start_min = min(start_min, start)
        end_max = max(end_max, end)

    try:
        span = doc[start:end].text
        preposition_of_time, datetime = span.split(" ", 1)
        return preposition_of_time, datetime
    except:
        return None, None


def parse_type_matches(matches, doc):
    if not matches:
        return None

    try:
        ticket_token_pos, type_token_pos = matches[0][1]
        type = doc[type_token_pos].lemma_
        return type if type in ["change", "audit", "create", "comment"] else None
    except:
        return None


