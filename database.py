import pandas as pd
from pandas import DataFrame
from datetime import datetime


class Database:
    def __init__(self, csv_path: str) -> None:
        # initialize data frame
        self.csv_path = csv_path
        self.df = self.init_dataframe()

    def init_dataframe(self) -> DataFrame:
        # load data frame from csv
        df = pd.read_csv(self.csv_path)
        df['created_at'] = pd.to_datetime(df['created_at'])
        df['type'] = df['type'].str.lower()
        return df

    def get_random_id(self) -> int:
        return self.df['author_id'].sample().values[0]

    def fetch_data(self, author_id: int, action: str, pot: str, time: datetime) -> str:
        if pot in ["before"]:
            df = self.df.loc[(self.df['author_id'] == author_id) & (self.df['created_at'] <= time)]
        elif pot in ["after", "since"]:
            df = self.df.loc[(self.df['author_id'] == author_id) & (self.df['created_at'] >= time)]
        elif pot in ["in", "on", "at"]:
            df = self.df.loc[(self.df['author_id'] == author_id) & (self.df['created_at'] == time)]

        if action in ["change", "audit", "comment", "create"]:
            df = df.loc[df['type'] == action]

        if df.empty:
            return "Sorry, no tickets found."

        return df[["id", "parent_id", "ticket_id", "author_id", "role", "created_at", "type"]]\
            .to_string(index=None, col_space=25)
