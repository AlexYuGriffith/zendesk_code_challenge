from PyQt5 import uic
from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5.QtCore import pyqtSlot

from nlp_model import RuleBasedModel
from database import Database
import dateparser
import click


class MainWindow(QMainWindow):
    def __init__(self, max_query_length: int, csv_path: str, nlp_package: str) -> None:
        super(MainWindow, self).__init__()
        uic.loadUi('main_window.ui', self)

        # Setup nlp model
        self.nlp_model = RuleBasedModel(nlp_package)

        # Setup database
        self.db = Database(csv_path)
        self.id = self.db.get_random_id()
        self.max_query_length = max_query_length

        # Setup UI
        self.setup_ui()

        # Setup actions
        self.btn_submit.clicked.connect(self.btn_submit_clicked)
        self.btn_update_id.clicked.connect(self.btn_update_id_clicked)

        # Display main window
        self.show()

    def setup_ui(self) -> None:
        uic.loadUi('main_window.ui', self)
        self.label_id.setText("Hi! User {} ".format(self.id))

    @pyqtSlot()
    def btn_update_id_clicked(self) -> None:
        self.id = self.db.get_random_id()
        self.label_id.setText("Hi! User {} ".format(self.id))

    @pyqtSlot()
    def btn_submit_clicked(self) -> None:
        query = self.label_query_input.text()

        if not query:
            self.text_browser_query_output.setText("Empty query! \n Please enter your request.")
            return

        if len(query) > self.max_query_length:
            self.text_browser_query_output.setText("Query exceeded maximum length! \n Please re-enter your request.")
            return

        type, pot, time = self.nlp_model.extract_info_tuple(query)

        if not type or not pot or not time:
            self.text_browser_query_output.setText("Invalid query! \n Please follow the suggested request format.")
        else:
            time = dateparser.parse(time)
            print(type, pot, time)
            self.text_browser_query_output.setText(self.db.fetch_data(self.id, type, pot, time))


@click.command(help="Zendesk Code Challenge")
@click.option("-l", "--max_query_length", type=int, default=100, help="maximum query length")
@click.option("-p", "--csv_path", type=str, default='./events_hw.csv', help="path to the csv file")
@click.option("-nlp", "--nlp_package", type=str, default="en_core_web_sm", help="nlp pipeline trained package")
def main(max_query_length, csv_path, nlp_package):
    app = QApplication([])
    app.setApplicationName("Zendesk Code Challenge")

    window = MainWindow(max_query_length, csv_path, nlp_package)
    app.exec_()


if __name__ == '__main__':
    main()

