import spacy
from spacy.matcher import Matcher, DependencyMatcher
from typing import Tuple

from utils import nlp_helper

"""
This rule-based nlp model extracts information tuple from the query by two steps:
1. Leverage the pre-trained nlp model to get an sequence of language tokens from the query.
2. Use hand-craft matching patterns to extract the information tuple from the sequence of language tokens.
"""
class RuleBasedModel:
    def __init__(self, nlp_package: str) -> None:
        self.nlp = spacy.load(nlp_package)  # load nlp pipeline trained package
        self.init_matcher()

    """
    This function initializes the time and action matchers, which will be used to extract
    information tuple (type, pot, time) from the query.
    """
    def init_matcher(self) -> None:
        # initialize action and time matchers
        self.type_matcher = DependencyMatcher(self.nlp.vocab)
        self.time_matcher = Matcher(self.nlp.vocab)
        # add time and action patterns to the matchers
        self.time_matcher.add("time_patterns", nlp_helper.time_patterns)
        self.type_matcher.add("action_pattern", [nlp_helper.type_pattern])

    """
    This function extracts information tuple from the input query text.
    type: different ticket type, like change, audit, comment and create.
    pot: preposition of time, like on, at, in, before, after and since. 
    time: any possible date or time.
    """
    def extract_info_tuple(self, txt: str) -> Tuple[str, str, str]:
        doc = self.nlp(txt)  # get an array of language tokens from query
        # extract information tuple by pattern matching
        time_matches = self.time_matcher(doc)
        type_matches = self.type_matcher(doc)
        pot, time = nlp_helper.parse_time_matches(time_matches, doc)
        type = nlp_helper.parse_type_matches(type_matches, doc)
        return type, pot, time


"""
If get enough training data of customer requests, a statistical nlp model can be trained to extract 
the information tuple from the query in an end-to-end machine learning manner. 
"""
class StatisticModel:
    pass

